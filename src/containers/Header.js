import React, { Component } from 'react';
import { Nav, Navbar, Form, Button} from 'react-bootstrap';
import logo from '../assets/img/logo.png';
class Header extends Component {
    render() {
        return (
            <div >
                <Navbar bg="light" expand="lg" fixed="top">
                    <Navbar.Brand href="/home"> 
                        <img
                            alt=""
                            src={logo}
                            width="100"
                            height="100"
                            className="d-inline-block  align-top nav-logo"
                        />{' '}
                    </Navbar.Brand>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto"/>
                            <Form inline>
                                <i aria-hidden="true" className="blue volume control phone icon "></i>
                                <label className="label-blue">Hubungi Kami 0818 7721 0099 </label>
                                <Button size="sm">Sign In</Button>
                            </Form>
                    </Navbar.Collapse>
                </Navbar>
               <Navbar className="bg-primary-2" expand="lg" variant="dark" fixed="top">
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="justify-content-center nav-2">
                            <Nav.Link className="link-1" href="/produk">Produk</Nav.Link>
                            <Nav.Link className="link-1" href="/cari-loan">Cari Loan Adviser</Nav.Link>
                            <Nav.Link className="link-1" href="#kalkulasi-simulasi">Kalkulasi Simulasi</Nav.Link>
                            <Nav.Link className="link-1" href="#kenapa-loan-market">Kenapa Loan Market</Nav.Link>
                            <Nav.Link className="link-1" href="#bergabung-bersama-kami">Bergabung Bersama Kami </Nav.Link>
                            <Nav.Link className="link-1" href="#berita">Berita</Nav.Link>
                            <Nav.Link className="link-1" href="#kontak-kami">Kontak Kami</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
             </div>
        );
    }
}

export default Header;