import React, { Component } from 'react';

import Pencarian from '../../component/Pencarian';
import MenuProduk from '../../component/MenuProduk';
import ContentProduk from '../../component/ContentKeuntunganKpr';
import JumboProduk from '../../component/Jumbotron/JumbotronKeuntungan';
import Footer from '../../containers/Footer';
import Header from '../../containers/Header';



class Keuntungan extends Component {
    render() {
        return (
            <div>
                <Header/>
                <JumboProduk/>
                <MenuProduk/>
                <ContentProduk/>
                <Pencarian/>
                <Footer/>
            </div>
        );
    }
}

export default Keuntungan;
