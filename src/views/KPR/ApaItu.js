import React, { Component } from 'react';

import CarouselPartner from '../../component/Carousel/PartnerProduk';
import Pencarian from '../../component/Pencarian';
import MenuProduk from '../../component/MenuProduk';
import ContentProduk from '../../component/ContentKpr';
import JumboProduk from '../../component/Jumbotron/JumbotronKpr';
import Footer from '../../containers/Footer';
import Header from '../../containers/Header';



class ApaItu extends Component {
    render() {
        return (
            <div>
                <Header/>
                <JumboProduk/>
                <MenuProduk/>
                <ContentProduk/>
                <CarouselPartner/>
                <Pencarian/>
                <Footer/>
            </div>
        );
    }
}

export default ApaItu;
