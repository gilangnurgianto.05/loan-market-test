import React, { Component } from 'react';

import CarouselPartner from '../component/Carousel/PartnerProduk';
import Pencarian from '../component/Pencarian';
import MenuProduk from '../component/MenuProduk';
import ContentProduk from '../component/ContentProduk';
import JumboProduk from '../component/Jumbotron/JumbotronProduk';
import Footer from '../containers/Footer';
import Header from '../containers/Header';



class Produk extends Component {
    render() {
        return (
            <div>
                <Header/>
                <JumboProduk/>
                <MenuProduk/>
                <ContentProduk/>
                <CarouselPartner/>
                <Pencarian/>
                <Footer/>
            </div>
        );
    }
}

export default Produk;
