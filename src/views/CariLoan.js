import React, { Component } from 'react';

import ContentCari from '../component/ContentCari';
import JumboProduk from '../component/Jumbotron/JumbotronCari';
import Footer from '../containers/Footer';
import Header from '../containers/Header';



class CariLoan extends Component {
    render() {
        return (
            <div>
                <Header/>
                <JumboProduk/>
                <ContentCari/>
                <Footer/>
            </div>
        );
    }
}

export default CariLoan;
