import React, { Component } from 'react';

import Pencarian from '../component/Pencarian';
import Footer from '../containers/Footer';
import Header from '../containers/Header';
import Content from '../component/ContentProfil';



class HasilPencarian extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Content/>
                <Pencarian/>
                <Footer/>
            </div>
        );
    }
}

export default HasilPencarian;
