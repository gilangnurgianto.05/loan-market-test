import React, { Component } from 'react';

import CarouselPartner from '../component/Carousel/PartnerHome';
import Pencarian from '../component/Pencarian';
import Informasi from '../component/Informasi';
import MenuHome from '../component/MenuHome';
import TopPerformance from '../component/TopPerformance';
import JumboHome from '../component/Jumbotron/JumbotronHome';
import News from '../component/News';
import Footer from '../containers/Footer';
import Header from '../containers/Header';



class Home extends Component {
    render() {
        return (
            <div>
                <Header/>
                <JumboHome/>
                <MenuHome/>
                <CarouselPartner/>
                <Informasi/>
                <TopPerformance/>
                <News/>
                <Pencarian/>
                <Footer/>
            </div>
        );
    }
}

export default Home;
