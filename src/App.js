import React from 'react';
import { Router, Route,Redirect, Switch} from 'react-router-dom';
import history from './history';
import Home from './views/Home';
import Produk from './views/Produk';
import Kpr from './views/KPR/ApaItu';
import KeuntunganKpr from './views/KPR/Keuntungan';
import CariLoan from './views/CariLoan';
import HasilCari from './views/HasilPencarian';
import ProfilLoan from './views/ProfilLoan';

function App() {
  return (
    <Router history={history}>
      <Switch>
          <Route exact path="/home" component={Home} />
          <Route  path="/produk" component={Produk} />
          <Route  path="/produk-kpr" component={Kpr} />
          <Route  path="/keuntungan-kpr" component={KeuntunganKpr} />
          <Route  path="/cari-loan" component={CariLoan} />
          <Route  path="/hasil-loan" component={HasilCari} />
          <Route  path="/Profil-loan" component={ProfilLoan} />
          <Redirect from="/" to="/home"/>
        </Switch>
    </Router>
  );
}

export default App;
