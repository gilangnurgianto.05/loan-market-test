import React, { Component } from 'react';
import {  Row, Container} from 'react-bootstrap';

class MenuHome extends Component {
    render() {
        return (
            <Container>
                <Row>
                    <label className="label-home">Produk Pinjaman Kami.</label>
                </Row>
                <Row className="justify-content-center">
                    <div className="card-menu bg-1 card-1"></div>
                    <div className="card-menu bg-2 card-1"></div>
                    <div className="card-menu bg-3 card-1"></div>
                    <div className="card-menu bg-4 card-1"></div>
                    <div className="card-menu bg-5 card-1"></div>
                </Row>   
            </Container>  
        );
    }
}

export default MenuHome;
