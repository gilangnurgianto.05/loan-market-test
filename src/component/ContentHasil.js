import React, { Component } from 'react';
import { Card, Button,  Row, Container, Form} from 'react-bootstrap';
import Profil01 from '../assets/img/profil-loan-01.png';
import Profil02 from '../assets/img/profil-loan-02.png';
import { withRouter } from 'react-router-dom';
import {history} from '../history';

class ContentHasil extends Component {
    handleProfil=()=>{
        this.props.history.push({pathname:"/profil-loan"})
    }
    render() {
        return ( 
        <Container>
            <Row className="row-hasil">
                <label className="label-hasil">2 Loan Adviser dengan nama Christina</label>
            </Row>
            <Row>
                <div className="card-hasil" bg="info">
                    <div className="m-2">
                         <Card.Img variant="top" src={Profil01} />
                    </div>
                    <Row>
                        <label className="nama-loan">Christina M. Jahja</label>
                    </Row>
                    <Row>
                        <label className="tempat-loan">Loan Market Alam Sutera</label>
                    </Row>
                    <Row className="justify-content-center">
                        <button className="button-profil" onClick={this.handleProfil}>Lihat Profil</button>
                    </Row>
                </div> 
                <div className="card-hasil" bg="info">
                    <div className="m-2">
                         <Card.Img variant="top" src={Profil02} />
                    </div>
                    <Row>
                        <label className="nama-loan">Christina Francisca</label>
                    </Row>
                    <Row>
                        <label className="tempat-loan">Loan Market Darmo Surabaya</label>
                    </Row>
                    <Row className="justify-content-center">
                        <button className="button-profil" onClick={this.handleProfil}>Lihat Profil</button>
                    </Row>
                </div>
            </Row>
        </Container>
        );
    }
}

export default withRouter(ContentHasil);
