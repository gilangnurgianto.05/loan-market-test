import React, { Component } from 'react';
import {  Row, Container} from 'react-bootstrap';

class MenuProduk extends Component {
    render() {
        return (
            <Row  className="bg-menu-produk">
                <Container>
                    <Row className="justify-content-center">
                        <div className="card-menu bg-6 card-1"></div>
                        <div className="card-menu bg-7 card-1"></div>
                    </Row>   
                </Container> 
            </Row>
        );
    }
}

export default MenuProduk;
