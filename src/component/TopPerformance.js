import React, { Component } from 'react';
import { Col, Row, Container} from 'react-bootstrap';

class TopPerformance extends Component {
    render() {
        return (
            <Container>
                <Row>
                    <label className="label-home">Congratulations Top Performers!</label>
                </Row>
                <Row>
                    <label className="label-date">May 2020</label>
                </Row>
                <Row className="justify-content-center">
                   <Col className="text-center">
                        <div className="card-top bg-top1"></div>
                   </Col>
                   <Col className="text-center">
                        <div className="card-top bg-top2"></div>
                   </Col>
                </Row>  
            </Container> 
        );
    }
}

export default TopPerformance;
