import React, { Component } from 'react';

import { Row, Carousel, Container } from 'react-bootstrap';
import partner1 from '../../assets/img/partner1.png'
import partner2 from '../../assets/img/partner2.png'
import partner3 from '../../assets/img/partner3.png'


class PartnerHome extends Component {
    constructor(props) {
      super(props);
      this.handleSelect = this.handleSelect.bind();
      this.state = {
          index: 0
      };
    }
  
     handleSelect = (selectedIndex) => {
      this.setState({index:selectedIndex});
    };

    render() {
        return (
          <Container>
            <Row>
                <label className="label-home">Partner Kami</label>
            </Row>
            <Carousel activeIndex={this.state.index} onSelect={this.handleSelect}>
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={partner1}
                    alt="First slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={partner2}
                    alt="Second slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={partner3}
                    alt="Third slide"
                  />
                </Carousel.Item>
            </Carousel>
          </Container>
        );
    }
}

export default PartnerHome;