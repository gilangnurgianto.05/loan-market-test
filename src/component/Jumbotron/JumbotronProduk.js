import React, { Component } from 'react';
import { Jumbotron, Container, Row} from 'react-bootstrap';

class JumbotronProduk extends Component {
    render() {
        return (
            <Jumbotron className="jumbotronProduk" fluid >
                <Container>
                    <Row>
                        <h1 className="label-j-produk">Own a home, <br/>invest in another</h1>
                    </Row>
                    <Row>
                        <p className="label-j">Who says you can't do both ?</p>
                    </Row>
                </Container>
            </Jumbotron>
        );
    }
}
export default JumbotronProduk;
