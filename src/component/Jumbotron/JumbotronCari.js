import React, { Component } from 'react';
import { Jumbotron, Container,Row} from 'react-bootstrap';

class JumbotronCari extends Component {
    render() {
        return (
            <Jumbotron className="jumbotronCari" fluid >
            <Container>
                <Row className="justify-content-center">
                    <p className="label-j-cari">Kesulitan saat mengajukan pinjaman?</p>
                </Row>
                <Row  className="justify-content-center">
                    <p  className="label-j-cari-h">Jangan khawatir, Loan Adviser kami siap membantu Anda.</p>
                </Row>
                <Row  className="justify-content-center">
                    <p  className="label-j-cari">Lebih dari 100 Loan Adviser yang tersebar di Indonesia siap menemui dan menghubungi Anda kapanpun.</p>
                </Row>
            </Container>
            </Jumbotron>
        );
    }
}

export default JumbotronCari;
