import React, { Component } from 'react';
import { Card, Button, Col, Row, Container, Form} from 'react-bootstrap';
import Profil02 from '../assets/img/profil-loan-02.png';

class ContentHasil extends Component {
    render() {
        return ( 
        <Container>
            <Row className="row-profil">
                <Col>
                    <Row>
                        <label className="label-profil">Christina Francisca</label>
                    </Row>
                    <Row>
                        <label className="label-profil-market">Loan Market Darmo Surabaya</label>
                    </Row>
                    <Row>
                        <label className="label-profil-isi">
                            She became Rookie at the 2nd Tri Annual Loan Market Awards 2018, she
                            was one of the outstanding Loan Advisers. She believes in doing the
                            right thing and to always keep her integrity. She is also responsible,
                            hardworking, humble and kind. She always keeps her eyes on the
                            target and do her best to achieve that.
                        </label>
                    </Row>
                    <Row>
                        <button className="button-janji">Membuat Janji</button>
                    </Row>
                    <Row>
                        <button className="button-kontak">Kontak Loan Advisor</button>
                    </Row>
                </Col>
                <Col>
                    <div className="card-profil">
                        <Card.Img variant="top" src={Profil02} />
                    </div>
                </Col>
            </Row>
        </Container>
        );
    }
}

export default ContentHasil;
