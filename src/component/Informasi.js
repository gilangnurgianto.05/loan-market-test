import React, { Component } from 'react';
import { Card, Col, Row, Container} from 'react-bootstrap';

class Informasi extends Component {
    render() {
        return (
            <Card bg="primary" text="white" className="card-blue">
                <Container>
                    <blockquote className="blockquote mb-0 card-body">
                    <label className="label-04">
                        Kenapa Harus Memilih Loan Market (dibanding langsung ke Bank)?
                    </label>
                    <label className="label-03">
                        Mencari pinjaman dapat menjadi sangat rumit dengan banyaknya pilihan yang tersedia.<br/>
                        Di Loan Market, mempermudah langkah Anda dalam membuat keputusan yang sesuai dengan kondisi dan kebutuhan Anda.
                    </label>
                    <div className="line-01"></div>
                    <Row className="justify-align-center">
                        <Row>
                            <Col className="col-2 ma-1 text-center" sm={4} xs={12}>
                                <i aria-hidden="true" className="white handshake huge icon"></i>
                            </Col>
                            <Col  className="col-10 ma-1" sm={8} xs={12}>
                                <label className="label-02">Kemampuan bernegosiasi dengan bank dan pemberi pinjaman.</label>
                                <label className="row-01">
                                    Loan Market bekerja sama dengan 27 lenders dan perusahaan multifinance. Pengalaman dan keahlian
                                    kami unggul dalam pemilihan produk pinjaman yang tepat untuk situasi dan kebutuhan Anda.
                                </label>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="col-2 ma-1 text-center" sm={4} xs={12}>
                                <i aria-hidden="true" className="white clipboard list huge icon"></i>
                            </Col>
                            <Col  className="col-10 ma-1" sm={8} xs={12}>
                                <label className="label-02">Penawaran produk kompetitif dan akses eksklusif.</label>
                                <label className="row-01">
                                    Kami mendapatkan akses khusus dari bank dan dapat membandingkan bunga, biaya, fasilitas dan
                                    kemudahan aplikasi yang cocok untuk Anda. Kami dapat membantu anda menghemat biaya dan waktu
                                    untuk mendapatkan pinjaman.
                                </label>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="col-2 ma-1 text-center" sm={4} xs={12}>
                                <i aria-hidden="true" className="white dollar sign huge icon"></i>
                            </Col>
                            <Col  className="col-10 ma-1" sm={8} xs={12}>
                                <label className="label-02">Konsultasi gratis dan tidak ada kewajiban untuk mengambil pinjaman.</label>
                                <label className="row-01">
                                    Loan Adviser kami akan memberikan konsultasi pinjaman yang bisa didapatkan setelah mendengarkan
                                    situasi dan kebutuhan Anda. Kami juga dapat mengkalkulasi kapasitas pinjaman yang bisa didapatkan dan
                                    cicilan Anda dengan akurat. Ditambah lagi, para konsultan berpengalaman kami akan mengecek dokumen
                                    Anda secara gratis sebelum diajukan ke bank.
                                </label>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="col-2 ma-1 text-center" sm={4} xs={12}>
                                <i aria-hidden="true" className="white check circle outline huge icon"></i> 
                            </Col>
                            <Col  className="col-10 ma-1" sm={8} xs={12}>
                                <label className="label-02">Terpercaya.</label>
                                <label className="row-01">
                                    Loan Market adalah rekanan resmi bank dengan network terluas di Indonesia dan terafiliasi dengan
                                    Ray White, agen properti nomor 1 di Indonesia.
                                </label>
                            </Col>
                        </Row>
                    </Row>
                    </blockquote>
                </Container>
            </Card>
        );
    }
}

export default Informasi;
