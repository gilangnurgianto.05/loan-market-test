import React, { Component } from 'react';
import {Row, Col, Container,Buttonv} from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import {history} from '../history';

class ContentKpr extends Component { 
    handleKpr=()=>{
        this.props.history.push({pathname:"/produk-kpr"})
    }
    handleKeuntungan=()=>{
        this.props.history.push({pathname:"/keuntungan-kpr"})
    }
    render() {
        return (
            <Container>
                <Row>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpr-active card-1" onClick={this.handleKpr}> 
                            <label className="label-kpr">APA ITU KPR ?</label>
                        </button>
                    </Col>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpr card-1" onClick={this.handleKeuntungan}>
                            <label className="label-kpr">KEUNTUNGAN KPR ?</label>
                        </button>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpa card-1" href="/produk-kpr">
                            <label className="label-kpr">APA ITU KPA ?</label>
                        </button>
                    </Col>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpa card-1">
                            <label className="label-kpr">KEUNTUNGAN KPA ?</label>
                        </button>
                    </Col>
                </Row>
                <Row>
                    <Row>
                        <label className="label-content-kpr">Kredit Pemilikan Rumah (KPR)</label>
                    </Row>
                    <Row>
                        <p className="isi-kpr">
                            KPR adalah hal yang penting dan merupakan komitmen finansial dengan jangka panjang, maka Anda harus mengetahui KPR secara detail dari awal
                            Dalam menentukan KPR yang tepat, Anda harus mengetahui sejumlah faktor yang mempengaruhi total plafond kredit Anda, dimulai dari harga
                            properti yang ingin Anda beli, pendapatan yang Anda miliki serta gaya hidup Anda.
                        </p>
                    </Row>
                    <Row>
                        <p className="isi-kpr">
                            Kredit Pemilikan Rumah (KPR) merupakan kredit yang digunakan untuk membeli rumah. Dengan fasilitas kredit yang diberikan, maka Anda dapat
                            membayar properti dengan mencicil angsuran. Anda cukup membayar uang muka sebesar 20% sampai dengan 30% dari harga properti kepada bank
                            atau penjual, maka Anda akan langsung bisa mendapatkan kredit rumah dengan mengangsur sisanya.
                        </p>
                    </Row>
                    <Row>
                        <p className="isi-kpr">
                            KPR bisa berupa kredit rumah primary, kredit rumah secondary, dan kredit take over. Keberadaan KPR sangat membantu dalam memudahkan Anda
                            untuk memiliki rumah atau properti yang ingin Anda miliki.
                        </p>
                    </Row>
                    <Row>
                        <p className="isi-kpr">
                            Dengan fasilitas berupa kredit yang diberikan, maka Anda dapat membayar dengan mencicil angsuran. Anda cukup membayar uang muka sebesar
                            20% sampai 30% dari harga properti kepada bank atau lembaga penjamin, maka Anda akan langsung bisa menjalankan kredit rumah dengan
                            mengangsur sisanya. Namun, patut diingat bahwa di dalam KPR terdapat bunga yang dibebankan pada peminjam. Sedangkan, untuk agunan yang
                            diperlukan untuk KPR adalah properti yang akan dibeli sendiri.
                        </p>
                    </Row>
                    <Row className="justify-content-center">
                        <Col className="text-center" xs={12} sm={6}>
                            <div className="card-syarat bg-syarat">
                                <Row>
                                    <p className="p-syarat">Syarat dan pengajuan KPR di Indonesia</p>
                                </Row>
                                <Row>
                                    <div>
                                        <div className="li-syarat">1. Warga Negara Indonesia (WNI) dan berdomisili di Indonesia.</div>
                                        <div className="li-syarat">2. Minimal usia pada saat kredit berakhir maksimal 55 tahun untuk pegawai dan maksimal 6 tahun untuk profesional/wiraswasta.</div>
                                        <div className="li-syarat">3. Memiliki pekerjaan dan penghasilan sebagai pegawai/wiraswasta/profesional dengan masa kerja/usaha minimal 1 tahun atau 2 tahun.</div>
                                    </div>

                                </Row>
                            </div>
                        </Col>
                        <Col className="text-center" xs={12} sm={6}>
                            <div className="card-syarat bg-dokument">
                                <Row>
                                    <p className="p-syarat">Dokumen penunjang yang dibutuhkan</p>
                                </Row>
                                <Row>
                                    <div>
                                        <div className="li-syarat">1. Bukti identitas diri yang masih berlaku berupa KTP/SIM/PASPOR</div >
                                        <div className="li-syarat">2. Kartu Keluarga</div >
                                        <div className="li-syarat">3. NPWP Pribadi</div >
                                        <div className="li-syarat">4. NPWP Perusahaan (untuk wiraswasta)</div >
                                        <div className="li-syarat">5. Surat Nikah atau Surat Cerai (bila sudah menikah atau bercerai)</div >
                                        <div className="li-syarat">6. Rekening tabungan 3 bulan terakhir (bila bekerja) atau Rekening Koran.</div >
                                        <div className="li-syarat">7. 3 bulan terakhir (bila wiraswasta)</div >
                                    </div>
                                </Row>
                            </div>
                        </Col>
                    </Row>  
                    <Row>
                        <button size="sm" className="btn-kembali" to="/kpr">Kembali</button>
                    </Row>

                </Row>
            </Container>
        );
    }
}

export default withRouter(ContentKpr);
