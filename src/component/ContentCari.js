import React, { Component } from 'react';
import { Card, Col,  Row, Container, Form} from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import {history} from '../history';

class ContentCari extends Component {
    handlePencarian=()=>{
        this.props.history.push({pathname:"/hasil-loan"})
    }
    render() {
        return ( 
        <Row bg="info" className="card-cari-content">
            <Container className="col-cari">
                <Col className="col-6  col-form" md={6} sm={6} xs={12} >
                    <Form className="col-pelanggan">
                        <Row>
                            <label className="label-form-cari">Baru mengenal Loan Market?</label>
                        </Row>
                        <Row>
                            <label  className="after-form-cari">Kami akan menyiapkan Loan Adviser untuk Anda</label>
                        </Row>
                        <Row >
                            <Col lg={6} sm={12} xs={12}>
                                <input className="form-input" placeholder="Nama Depan" />
                            </Col>
                            <Col lg={6} sm={12} xs={12}>
                                <input className="form-input" placeholder="Nama Belakang" />
                            </Col>
                        </Row>
                        <Row > 
                            <Col>
                                <input className="form-input" placeholder="Alamat Email" />
                            </Col>
                        </Row>
                        <Row  >
                            <Col lg={6} sm={12} xs={12}>
                                <input className="form-input" placeholder="Nomor Telepon" />
                            </Col>
                            <Col lg={6} sm={12} xs={12}>
                                <input className="form-input" placeholder="Kode Pos" />
                            </Col>
                        </Row>
                        <Row> 
                            <button className="button-cari" onClick={this.handlePencarian}>Selanjutnya</button>
                        </Row>
                        <Row>
                            <label className="label-form-footer">Dengan mengisi form ini, saya telah menerima syarat & ketentuan dan kebijakan privasi</label>
                        </Row>
                    </Form>
                </Col>
                <Col className="col-6  col-form" md={6} sm={6} xs={12}>
                    <Form>
                        <Row>
                            <label className="label-form-cari m-9">Pelanggan yang ada</label>
                        </Row>
                        <Row>
                            <label  className="after-form-cari">Cari Loan Adviser berdasarkan nama</label>
                        </Row>
                        <Row> 
                            <Col>
                                <input className="form-input" placeholder="Nama Loan Adviser" />
                            </Col>
                        </Row>
                        <Row> 
                            <Col>
                                <button className="button-cari-loan" onClick={this.handlePencarian}>Temukan Loan Adviser</button>
                            </Col>
                        </Row>
                    </Form>
                </Col> 
            </Container> 
        </Row>
        );
    }
}

export default withRouter(ContentCari);
