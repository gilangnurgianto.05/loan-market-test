import React, { Component } from 'react';
import {Row, Col, Container} from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import {history} from '../history';

class ContentProduk extends Component {
    handleKpr=()=>{
        this.props.history.push({pathname:"/produk-kpr"})
    }
    handleKeuntungan=()=>{
        this.props.history.push({pathname:"/keuntungan-kpr"})
    }
    render() {
        return (
            <Container>
                <Row>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpr card-1" onClick={this.handleKpr}>
                            <label className="label-kpr">APA ITU KPR ?</label>
                        </button>
                    </Col>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpr card-1" onClick={this.handleKeuntungan}>
                            <label className="label-kpr">KEUNTUNGAN KPR ?</label>
                        </button>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpa card-1">
                            <label className="label-kpr">APA ITU KPA ?</label>
                        </button>
                    </Col>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpa card-1">
                            <label className="label-kpr">KEUNTUNGAN KPA ?</label>
                        </button>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default withRouter(ContentProduk);
