import React, { Component } from 'react';
import {Row, Col, Container,Button} from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import {history} from '../history';

class ContentKeuntunganKpr extends Component { 
    handleKpr=()=>{
        this.props.history.push({pathname:"/produk-kpr"})
    }
    handleKeuntungan=()=>{
        this.props.history.push({pathname:"/keuntungan-kpr"})
    }
    handleKembali=()=>{
        this.props.history.push({pathname:"/produk"})
    }
    render() {
        return (
            <Container>
                <Row>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpr card-1" onClick={this.handleKpr}>
                            <label className="label-kpr">APA ITU KPR ?</label>
                        </button>
                    </Col>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpr-active card-1" onClick={this.handleKeuntungan}>
                            <label className="label-kpr">KEUNTUNGAN KPR ?</label>
                        </button>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpa card-1" href="/produk-kpr">
                            <label className="label-kpr">APA ITU KPA ?</label>
                        </button>
                    </Col>
                    <Col className="text-center col-6">
                        <button className="card-menu-kpr bg-kpa card-1">
                            <label className="label-kpr">KEUNTUNGAN KPA ?</label>
                        </button>
                    </Col>
                </Row>
                <Row>
                    <Row>
                        <label className="label-content-kpr">Keuntungan KPR melalui Loan Market</label>
                    </Row>
                    <Row>
                        <p className="isi-kpr">
                            Umumnya, para pengembang telah bekerja sama dengan bank-bank untuk dapat menciptakan proses pengajuan kredit yang lebih baik serta lancar. Maka
                            dari itu, dalam mengajukan kredit, Anda perlu cermat ketika memilih bank yang tepat untuk melakukan pengajuan. Loan Market siap dan mampu membantu
                            Anda dalam proses pengajuan aplikasi kredit kepemilikan rumah yang Anda impikan dengan membantu Anda mendapatkan pinjaman KPR dari bank – bank
                            terkemuka yang paling sesuai dengan kondisi Anda.
                        </p>
                    </Row>
                    <Row>
                        <p className="isi-kpr">
                            Loan Adviser kami siap membantu Anda secara langsung dalam membandingkan bunga, biaya, dan pengeluaran lainnya yang tujuannya untuk memastikan
                            pinjaman KPR yang sesuai dengan Anda.
                        </p>
                    </Row>
                    <Row>
                        <p className="isi-kpr">
                            Dalam memberikan pinjaman, pemberi pinjaman akan meminta sejumlah informasi tambahan terkait diri Anda. Tentunya, Loan Adviser Anda akan siap
                            memberitahu Anda apa yang akan diperlukan selama menjalani proses aplikasi pinjaman.
                        </p>
                    </Row>
                    <Row>
                        <button size="sm" className="btn-kembali" onClick={this.handleKembali}>Kembali</button>
                    </Row>

                </Row>
            </Container>
        );
    }
}

export default withRouter(ContentKeuntunganKpr);
