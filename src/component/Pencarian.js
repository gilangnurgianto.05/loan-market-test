import React, { Component } from 'react';
import { Card, Col,  Row, Container} from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import {history} from '../history';

class Pencarian extends Component {
    handlePencarian=()=>{
        this.props.history.push({pathname:"/cari-loan"})
    }
    render() {
        return ( 
        <Card bg="primary" text="white" className="card-cari">
            <Container>
                <Row  className="justify-content-center">
                    <label className="label-cari">Mulai Mencari</label>
                </Row>
                <Row>
                    <Col className="col-6">
                        <button className="btn-simulasi" onClick={this.handlePencarian}>Kalkulator Simulasi</button>
                    </Col>
                    <Col className="col-6">
                        <button className="btn-bergabung" onClick={this.handlePencarian}>Bergabung Bersama Kami</button>
                    </Col>
                </Row>
            </Container> 
        </Card>
        );
    }
}

export default withRouter(Pencarian);
