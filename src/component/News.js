import React, { Component } from 'react';
import {  Col, Row, Container} from 'react-bootstrap';

class News extends Component {
    render() {
        return (
            <Container>
                <Row >
                    <Col className="col-3 col-pulse">
                        <Row>
                            <label className="label-c-pulse">The Pulse</label>
                        </Row >
                        <Row className="line-02">
                            <Col >
                                <Row className="justify-content-center">
                                    <div className="card-pulse bg-pulse"></div>
                                </Row>
                                <Row className="justify-content-center ">
                                    <label className="label-pulse">Inflasi dan Kepemilikan Rumah</label>
                                </Row>
                                <Row className="justify-content-center">
                                    <label className="label-pulse-isi">Bank Indonesia (BI) Memprediksi harga properesidensial kuartal II - 2019 akan meningkat . . . .</label>
                                </Row> 
                                <Row>
                                    <label className="label-pulse-read">Read More. . . .</label>
                                </Row>
                            </Col>
                        </Row>
                    </Col> 
                    <Col className="col-9 col-pulse">
                        <Row >
                            <label className="label-c-pulse">Loan Market News</label>
                        </Row>
                        <Row>
                            <Col>
                                <Row className="justify-content-center">
                                    <div className="card-pulse bg-pulse"></div>
                                </Row>
                                <Row className="justify-content-center ">
                                    <label className="label-pulse">Inflasi dan Kepemilikan Rumah</label>
                                </Row>
                                <Row className="justify-content-center">
                                    <label className="label-pulse-isi">Bank Indonesia (BI) Memprediksi harga properesidensial kuartal II - 2019 akan meningkat . . . .</label>
                                </Row> 
                                <Row>
                                    <label className="label-pulse-read">Read More. . . .</label>
                                </Row>
                            </Col>
                            <Col>
                                <Row className="justify-content-center">
                                    <div className="card-pulse bg-pulse"></div>
                                </Row>
                                <Row className="justify-content-center">
                                    <label className="label-pulse">Inflasi dan Kepemilikan Rumah</label>
                                </Row>
                                <Row className="justify-content-center">
                                    <label className="label-pulse-isi">Bank Indonesia (BI) Memprediksi harga properesidensial kuartal II - 2019 akan meningkat . . . .</label>
                                </Row> 
                                <Row>
                                    <label className="label-pulse-read">Read More. . . .</label>
                                </Row>
                            </Col> 
                            <Col>
                                <Row className="justify-content-center">
                                    <div className="card-pulse bg-pulse"></div>
                                </Row>
                                <Row className="justify-content-center">
                                    <label className="label-pulse">Inflasi dan Kepemilikan Rumah</label>
                                </Row>
                                <Row className="justify-content-center">
                                    <label className="label-pulse-isi">Bank Indonesia (BI) Memprediksi harga properesidensial kuartal II - 2019 akan meningkat . . . .</label>
                                </Row> 
                                <Row>
                                    <label className="label-pulse-read">Read More. . . .</label>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container> 
        );
    }
}

export default News;
